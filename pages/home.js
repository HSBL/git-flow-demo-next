import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <h1>Halaman awal yang diakses pengunjung yang login</h1>
    </div>
  );
}
